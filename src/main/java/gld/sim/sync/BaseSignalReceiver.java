/*
 * Created on Sep 5, 2004
 * Universidad Tecnológica Nacional - Facultad Regional Buenos Aires
 * Proyecto Final 2004
 * I-ATraCoS - Grupo :
 * - Brey, Gustavo
 * - Escobar, Gaston
 * - Espinosa, Marisa
 * - Pastorino, Marcelo
 * All right reserved
 * Class Description
 * 
 */
package gld.sim.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.ResourceBundle;



/**
* CVS Log
* $Id: BaseSignalReceiver.java,v 1.4 2004/10/18 02:49:14 gescobar Exp $
* @author $Author: gescobar $
* @version $Revision: 1.4 $
**/
public abstract class BaseSignalReceiver implements SyncSignalReceiver{
	
	
	public abstract HashMap getSignalInfo(String sessionId, ResourceBundle rb) throws IOException;
	protected ResourceBundle rb = null;
	
	
	public void setResourceBundle(ResourceBundle _rb) {
		rb = _rb;
	
	}
	
}
