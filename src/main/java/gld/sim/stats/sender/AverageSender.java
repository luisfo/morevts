/*
 * Created on Sep 5, 2004
 * Universidad Tecnológica Nacional - Facultad Regional Buenos Aires
 * Proyecto Final 2004
 * I-ATraCoS - Grupo :
 * - Brey, Gustavo
 * - Escobar, Gaston
 * - Espinosa, Marisa
 * - Pastorino, Marcelo
 * All right reserved
 * Class Description
 * 
 */
package gld.sim.stats.sender;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;



/**
* CVS Log
* $Id: AverageSender.java,v 1.4 2004/10/08 12:12:41 gescobar Exp $
* @author $Author: gescobar $
* @version $Revision: 1.4 $
**/
public interface AverageSender{
	public PrintWriter getSender() throws UnknownHostException , IOException ;
	public void closeSender(PrintWriter out);
}
