
/*-----------------------------------------------------------------------
 * Copyright (C) 2001 Green Light District Team, Utrecht University 
 * Copyright of the TC2 algorithm (C) Marco Wiering, Utrecht University
 *
 * This program (Green Light District) is free software.
 * You may redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by
 * the Free Software Foundation (version 2 or later).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * See the documentation of Green Light District for further information.
 *------------------------------------------------------------------------*/

package gld.algo.tlc;

import gld.*;
import gld.sim.*;
import gld.algo.tlc.*;
import gld.infra.*;
import gld.utils.*;
import gld.xml.*;

import java.io.IOException;
import java.util.*;
import java.awt.Point;

/**
 *
 * This controller will decide it's Q values for the traffic lights according to the traffic situation on
 * the lane connected to the TrafficLight. It will learn how to alter it's outcome by reinforcement learning.
 *
 * @author Arne K, Jilles V
 * @version 0.912
 */
public class TC2TLCOpt extends TLController implements Colearning,InstantiationAssistant
{	
	protected Infrastructure infrastructure;
	protected TrafficLight[][] tls;
	protected Node[] allnodes;
	protected int num_nodes;
	
	protected Vector[][][] count, p_table, pKtl_table;
	protected float [][][][] q_table; //sign, pos, des, color (red=0, green=1)
	protected float [][][]   v_table;
	protected static float gamma=0.95f;				//Discount Factor; used to decrease the influence of previous V values, that's why: 0 < gamma < 1
	protected final static boolean red=false, green=true;
	protected final static int green_index=0, red_index=1;
	public final static String shortXMLName="tlc-tc2opt";
	protected static float random_chance=0.01f;				//A random gain setting is chosen instead of the on the TLC dictates with this chance
	private Random random_number;
	
	/**
	 * The constructor for TL controllers
	 * @param The model being used.
	 */
 
	public TC2TLCOpt( Infrastructure infra ) throws InfraException
	{	super(infra);
	}
	
	public void setInfrastructure(Infrastructure infra)
	{	super.setInfrastructure(infra);
		Node[] nodes = infra.getAllNodes();
		num_nodes = nodes.length;
		
		int numSigns = 0;
		
		try { numSigns = infra.getAllInboundLanes().size(); }
		catch(Exception e) {}
		
		q_table = new float [numSigns][][][];
		v_table = new float [numSigns][][];
		count	= new Vector[numSigns][][];
		p_table	= new Vector[numSigns][][];
		pKtl_table = new Vector[numSigns][][];
		
		int num_specialnodes = infra.getNumSpecialNodes();
		
		for (int i=0; i<num_nodes; i++) {
			Node n = nodes[i];
			
			int num_dls = 0;
			Drivelane[] dls = null;
			try {
				dls = n.getInboundLanes();
				num_dls = dls.length;
			}
			catch(Exception e) {}			
			
			for (int j=0; j<num_dls; j++) {
				Drivelane d = dls[j];
				Sign s = d.getSign();
				int id = s.getId();
				int num_pos = d.getCompleteLength();
				q_table[id] = new float [num_pos][][];
				v_table[id] = new float [num_pos][];
				p_table[id] = new Vector[num_pos][];
				count[id] = new Vector[num_pos][];
				pKtl_table[id] = new Vector[num_pos][];
				
				for (int k=0; k<num_pos; k++) {
					q_table[id][k]=new float[num_specialnodes][2];
					v_table[id][k]=new float[num_specialnodes];
					p_table[id][k] = new Vector[num_specialnodes];
					count[id][k] = new Vector[num_specialnodes];
					pKtl_table[id][k] = new Vector[num_specialnodes];
					
					for (int l=0; l<num_specialnodes;l++) {
						q_table[id][k][l][0]=0.0f;
						q_table[id][k][l][1]=0.0f;
						v_table[id][k][l]=0.0f;
						p_table[id][k][l] = new Vector();
						count[id][k][l] = new Vector();
						pKtl_table[id][k][l] = new Vector();
					}
				}
			}
		}
		random_number = new Random();
	}

	
	/**
	* Calculates how every traffic light should be switched
	* Per node, per sign the waiting roadusers are passed and per each roaduser the gain is calculated.
	* @param The TLDecision is a tuple consisting of a traffic light and a reward (Q) value, for it to be green
	* @see gld.algo.tlc.TLDecision
	*/	
	public TLDecision[][] decideTLs()
	{
	    int currenttlID, waitingsize, pos, destID, num_dec;
	    float gain = 0;
	    Sign currenttl;
	    Drivelane currentlane;
	    ListIterator queue;
	    Roaduser ru;
	    
		//Determine wheter it should be random or not
		boolean do_this_random = false;
		if (random_number.nextFloat() < random_chance) do_this_random = true;
	    
		for (int i=0;i<num_nodes;i++) {
			num_dec = tld[i].length;
			for(int j=0;j<num_dec;j++) {
				currenttl = tld[i][j].getTL();
				currenttlID = currenttl.getId();
				gain=0;
	    		
				currentlane = currenttl.getLane();
				waitingsize = currentlane.getNumRoadusersWaiting();
				queue = currentlane.getCompleteQueue().listIterator();
	    		if(!do_this_random) {
					for(; waitingsize>0; waitingsize--) {
						ru = (Roaduser) queue.next();
						pos = ru.getPosition();
						destID = ru.getDestNode().getId();
		    			
						gain += q_table[currenttlID][pos][destID][red_index] - q_table[currenttlID][pos][destID][green_index];  //red - green
					}
					if(gain >50.0f || gain < -50.0f) System.out.println("gain too low/high: "+gain);
                }
                else gain = random_number.nextFloat();
				tld[i][j].setQValue(gain);
			}
		}
		return tld;
	}

	public void updateRoaduserMove(Roaduser ru, Drivelane prevlane, Sign prevsign, int prevpos, Drivelane dlanenow, Sign signnow, int posnow, Drivelane[] possiblelanes, Point[] ranges, Drivelane desired)
	{
		//When a roaduser leaves the city; this will 
		if(dlanenow == null || signnow == null) {
			return;
		}
		
		//This ordening is important for the execution of the algorithm!
		int Ktl = dlanenow.getNumRoadusersWaiting();
		if(prevsign.getType()==Sign.TRAFFICLIGHT && (signnow.getType()==Sign.TRAFFICLIGHT || signnow.getType()==Sign.NO_SIGN)) {
			Node dest = ru.getDestNode();
			recalcP(prevsign, prevpos, dest, prevsign.mayDrive(), signnow, posnow, Ktl);
			recalcQ(prevsign, prevpos, dest, prevsign.mayDrive(), signnow, posnow, possiblelanes, ranges, Ktl);
			recalcV(prevsign, prevpos, dest);
		}
	}
	
	protected void recalcP(Sign tl, int pos, Node destination, boolean light, Sign tl_new, int pos_new, int Ktl)
	{
		int tlId = tl.getId();
		int desId = destination.getId();
		//Update the count table
		CountEntry currentsituation = new  CountEntry(tl, pos, destination, light, tl_new, pos_new, Ktl);
		int count_index = count[tlId][pos][desId].indexOf(currentsituation);
		if (count_index>=0) {
			CountEntry temp = (CountEntry) count[tl.getId()][pos][destination.getId()].elementAt(count_index);
			temp.incrementValue();
			currentsituation = temp;
		}
		else {
			count[tlId][pos][desId].add(currentsituation);
		}
		//Update the p_table
		PEntry currentchance = new PEntry(tl, pos, destination, light, tl_new, pos_new);		
		
		int dest=0, source=0;
		
		Enumeration _enum = count[tlId][pos][desId].elements();
		while(_enum.hasMoreElements()) {
			CountEntry current = (CountEntry) _enum.nextElement();
			dest += current.sameSourceDifferentKtl(currentsituation);
			source += current.sameSource(currentsituation);
		}

		if(source == 0) currentchance.setValue(0);
		else currentchance.setValue((float)dest/(float)source);
		
		int p_index = p_table[tlId][pos][desId].indexOf(currentchance);
		if(p_index>=0) p_table[tlId][pos][desId].setElementAt(currentchance, p_index);
		else {
			p_table[tlId][pos][desId].add(currentchance);
			p_index = p_table[tlId][pos][desId].indexOf(currentchance);
		}

		// Change the rest of the p_table, Also check the other chances for updates
		int size = p_table[tlId][pos][desId].size()-1;
		for(; size>=0; size--) {
			PEntry P = (PEntry) p_table[tlId][pos][desId].elementAt(size);
			float pvalue = P.sameSource(currentsituation);
			if(pvalue > -1.0f) {
				if(size != p_index)
					P.setValue(pvalue * (float)(source-1) / (float)source);
			}
		}

		//update the p'_table ......		
		PKtlEntry currentchance2 = new PKtlEntry(tl, pos, destination, light, tl_new, pos_new, Ktl);		
		source=0;
				
		_enum = count[tlId][pos][desId].elements();
		while(_enum.hasMoreElements()) {
			source += ((CountEntry) _enum.nextElement()).sameSourceWithKtl(currentsituation);
		}
		
		dest = currentsituation.getValue();
		if(source == 0) currentchance2.setValue(0);
		else currentchance2.setValue((float)dest/(float)source);
	
		p_index = pKtl_table[tlId][pos][desId].indexOf(currentchance2);
		if(p_index>=0) pKtl_table[tlId][pos][desId].setElementAt(currentchance2, p_index);
		else {
			pKtl_table[tlId][pos][desId].add(currentchance2);
			p_index = pKtl_table[tlId][pos][desId].indexOf(currentchance2);
		}
		
		// Change the rest of the pKtl_table, Also check the other chances for updates
		size = pKtl_table[tlId][pos][desId].size()-1;
		for(; size>=0; size--) {
			PKtlEntry P = (PKtlEntry) pKtl_table[tlId][pos][desId].elementAt(size);
			float pvalue = P.sameSource(currentsituation);
			if(pvalue > -1) {
				if(size != p_index) {
					P.setValue(pvalue * (float)(source-1) / (float)source);
				}
			}
		}

		if(currentchance.getValue() >1  ||currentchance2.getValue() >1 || currentchance.getValue() <0  ||currentchance2.getValue() <0 )	System.out.println("Serious error !!!!!!!!!1");
	}
	
	protected void recalcQ(Sign tl, int pos, Node destination, boolean light, Sign tl_new, int pos_new, Drivelane[] possiblelanes, Point[] ranges, int Ktl)
	{
		/*	Recalculate the Q values, only one PEntry has changed, meaning also only 1 QEntry has to change
		*/

		float newQvalue=0;
		Target[] targets = ownedTargets(tl, pos, destination, light);
		int R;
		int destId = destination.getId();
		float V=0;
		float test_sum_chance=0;
		
		if(!light) {	
			CountEntry currentsituation = new  CountEntry(tl, pos, destination, light, tl_new, pos_new, Ktl);
			int size = p_table[tl.getId()][pos][destination.getId()].size()-1;
			for(; size>=0; size--)
			{
				PEntry P = (PEntry) p_table[tl.getId()][pos][destination.getId()].elementAt(size);
				if(P.sameSource(currentsituation) > -1.0f)
				{
					//PEntry P = new PEntry(tl, pos, destination, light, targets[target_index].getTL(), targets[target_index].getP());			
					R = rewardFunction(tl_new, pos_new, possiblelanes, ranges);
					try {
						V = v_table[P.tl_new.getId()][P.pos_new][destId];
					}
					catch (Exception e) {
						System.out.println("ERROR");
					}
					newQvalue += P.getValue() *(R + (gamma * V));
				}
			}
		}
		else
		{
			int num_targets = targets.length;
			
			for(int target_index=0; target_index<num_targets; target_index++) {
				PKtlEntry P = new PKtlEntry(tl, pos, destination, light, targets[target_index].getTL(), targets[target_index].getP(), Ktl);	
				int p_index = p_table[tl.getId()][pos][destination.getId()].indexOf(P);
				
				if(p_index>=0) {
					P = (PKtlEntry) pKtl_table[tl.getId()][pos][destination.getId()].elementAt(p_index);
					R = rewardFunction(tl_new, pos_new, possiblelanes, ranges);
					try {
						V = v_table[targets[target_index].getTL().getId()][targets[target_index].getP()][destId];
					}
					catch (Exception e) {
						System.out.println("ERROR");
					}					
					newQvalue += P.getValue() *(R + (gamma * V));
				}
			}
		}
		q_table[tl.getId()][pos][destId][light?green_index:red_index]=newQvalue;
	}

	protected void recalcV(Sign tl, int pos, Node destination)
	{
		int tlId = tl.getId();
		int desId = destination.getId();
		float newVvalue;
		float q_red = q_table[tlId][pos][desId][red_index];
		float q_green = q_table[tlId][pos][desId][green_index];
		int[] amount = count(tl, pos, destination);
		
		//See the green_index definitions above !!!!
		float amount_green = (float) amount[green_index];
		float amount_red = (float) amount[red_index];
		
		newVvalue = ((float)amount_green/(float)(amount_green+amount_red))*q_green + ((float)amount_red/(float)(amount_green+amount_red))*q_red;

		v_table[tlId][pos][desId]=newVvalue;
	}

	/*
				==========================================================================
							Additional methods, used by the recalc methods 
				==========================================================================
	*/

	protected int[] count(Sign tl, int pos, Node destination)
	{
		int[] counters;
		counters = new int[2];
		
		//See the green_index definitions above !!!!
		counters[green_index] = 0;
		counters[red_index] = 0;
		
		//Calcs the number of entries in the table matching the given characteristics, and returns the count
		int psize = p_table[tl.getId()][pos][destination.getId()].size()-1;
		for(; psize>=0; psize--)
		{
			PEntry candidate = (PEntry) p_table[tl.getId()][pos][destination.getId()].elementAt(psize);
			if(candidate.tl.getId() == tl.getId() && candidate.pos == pos && candidate.destination.getId() ==destination.getId()) {
					if(candidate.light == green) {
						counters[green_index]++;
					}
					else {
						counters[red_index]++;
					}
			}
		}
		return counters;
	}
	
	protected int rewardFunction(Sign tl_new, int pos_new, Drivelane[] possiblelanes, Point[] ranges)
	{
		//Ok, the reward function is actually very simple; it searches for the tuple (tl_new, pos_new) in the given set
		int size = possiblelanes.length;
		
		for(int i=0; i<size; i++)	{
			if( possiblelanes[i].getId()==tl_new.getId()) {
				if(ranges[i].x < pos_new)	{
					if(ranges[i].y > pos_new)	{
						return 0;
					}
				}
			}
		}
		return 1;
	}
	
	protected Target[] ownedTargets(Sign tl, int pos, Node des, boolean light)
	{
		//This method will determine to which destinations you can go starting at this source represented in this QEntry
		
		CountEntry dummy = new CountEntry(tl, pos, des, light, tl, pos, 0);
		Target[] ownedtargets;
		Vector candidate_targets;
		candidate_targets = new Vector();
		
		//Use the count table to sort this out, we need all Targets from 
		//Only the elements in the count table are used, other  just give a P
		
		Enumeration _enum = count[tl.getId()][pos][des.getId()].elements();
		while(_enum.hasMoreElements()) {
			CountEntry current_entry = (CountEntry) _enum.nextElement();
			if(current_entry.sameSource(dummy) != 0) {	
				candidate_targets.addElement(new Target(current_entry.tl_new , current_entry.pos_new));
			}
		}
		ownedtargets = new Target[candidate_targets.size()];
		candidate_targets.copyInto(ownedtargets);
		return ownedtargets;
	}

	public float getVValue(Sign sign, Node des, int pos)
	{
		return v_table[sign.getId()][pos][des.getId()];
	}


	public float getColearnValue(Sign sign_new, Sign sign, Node destination, int pos)
	{
		int Ktl = sign.getLane().getNumRoadusersWaiting();
	
		// Calculate the colearning value
		float newCovalue=0;
		int size = sign.getLane().getCompleteLength()-1;

		for(; size>=0; size--) {
			float V;
			PKtlEntry P = new PKtlEntry(sign, 0, destination, green, sign_new, size, Ktl);
			int p_index = pKtl_table[sign.getId()][0][destination.getId()].indexOf(P);
			
			if(p_index>=0) {
				try {
					P = (PKtlEntry) pKtl_table[sign.getId()][0][destination.getId()].elementAt(p_index);
					V = v_table[sign.getId()][size][destination.getId()];
					newCovalue += P.getValue() * V;
				}
				catch (Exception e) {
					System.out.println("Error");
				}
			}
		}
		return newCovalue;
	}
	

	/*
				==========================================================================
					Internal Classes to provide a way to put entries into the tables 
				==========================================================================
	*/

	public class CountEntry implements XMLSerializable , TwoStageLoader
	{
		Sign tl;
		int pos;
		Node destination;
		boolean light;
		Sign tl_new;
		int pos_new;
		int Ktl;
		int value;
		String parentName="model.tlc";
		TwoStageLoaderData loadData=new TwoStageLoaderData();
		
		CountEntry(Sign _tl, int _pos, Node _destination, boolean _light, Sign _tl_new, int _pos_new, int _Ktl) {
			tl = _tl;
			pos = _pos;
			destination = _destination;
			light = _light;
			tl_new = _tl_new;
			pos_new = _pos_new;
			Ktl = _Ktl;
			value=1;
		}
		
		CountEntry ()
		{ // Empty constructor for loading
		}
		
		public void incrementValue() {
			value++;
		}
		
		public int getValue() {
			return value;
		}

		public boolean equals(Object other) {
			if(other != null && other instanceof CountEntry) {
				CountEntry countnew = (CountEntry) other;
				if(countnew.tl.getId()!=tl.getId()) return false;
				if(countnew.pos!=pos) return false;
				if(countnew.destination.getId()!=destination.getId()) return false;
				if(countnew.light!=light) return false;
				if(countnew.tl_new.getId()!=tl_new.getId()) return false;
				if(countnew.pos_new!=pos_new) return false;
				if(countnew.Ktl!=Ktl) return false;
				return true;
			}
			return false;
		}

		public int sameSource(CountEntry other) {
			if(other.tl.getId()==tl.getId() && other.pos == pos && other.light==light && other.destination.getId()==destination.getId()) {
				return value;
			}
			else {
				return 0;
			}
		}

		public int sameSourceDifferentKtl(CountEntry other) {
			if(other.tl.getId()==tl.getId() && other.pos == pos && other.light==light && other.destination.getId()==destination.getId() && other.tl_new.getId()==tl_new.getId() && other.pos_new == pos_new) {
				return value;
			}
			else {
				return 0;
			}
		}
		
		public int sameSourceWithKtl(CountEntry other) {
			if(other.tl.getId()==tl.getId() && other.pos == pos && other.light==light && other.destination.getId()==destination.getId() && other.Ktl == Ktl) {
				return value;
			}
			else {
				return 0;
			}
		}

		// XMLSerializable implementation of CountEntry
		
		public void load (XMLElement myElement,XMLLoader loader) throws XMLTreeException,IOException,XMLInvalidInputException
		{	pos=myElement.getAttribute("pos").getIntValue();
		   	loadData.oldTlId=myElement.getAttribute("tl-id").getIntValue();
		   	loadData.destNodeId=myElement.getAttribute("destination").getIntValue();
		   	light=myElement.getAttribute("light").getBoolValue();
			loadData.newTlId=myElement.getAttribute("newtl-id").getIntValue();
			pos_new=myElement.getAttribute("new-pos").getIntValue();
			Ktl=myElement.getAttribute("ktl").getIntValue();
			value=myElement.getAttribute("value").getIntValue(); 
		}

		public XMLElement saveSelf () throws XMLCannotSaveException
		{ 	XMLElement result=new XMLElement("count");
			result.addAttribute(new XMLAttribute("tl-id",tl.getId()));
			result.addAttribute(new XMLAttribute("pos",pos));
			result.addAttribute(new	XMLAttribute("destination",destination.getId()));
			result.addAttribute(new XMLAttribute("light",light));
			result.addAttribute(new XMLAttribute("newtl-id",tl_new.getId()));
			result.addAttribute(new XMLAttribute("new-pos",pos_new));
			result.addAttribute(new XMLAttribute("ktl",Ktl));
			result.addAttribute(new XMLAttribute("value",value));
	  		return result;
		}
  
		public void saveChilds (XMLSaver saver) throws XMLTreeException,IOException,XMLCannotSaveException
		{ 	// A count entry has no child objects
		}

		public String getXMLName ()
		{ 	return parentName+".count";
		}
		
		public void setParentName (String parentName)
		{	this.parentName=parentName; 
		}
	
		// TwoStageLoader implementation of CountEntry

		class TwoStageLoaderData 
		{ 	int oldTlId,newTlId,destNodeId;
		}
		
		public void loadSecondStage (Dictionary dictionaries)
		{ 	Dictionary laneDictionary=(Dictionary)(dictionaries.get("lane")),
		             nodeDictionary=(Dictionary)(dictionaries.get("node"));
		  	tl=((Drivelane)(laneDictionary.get(
		      		new Integer(loadData.oldTlId)))).getSign();
		  	tl_new=((Drivelane)(laneDictionary.get(
		      		new Integer(loadData.newTlId)))).getSign();
		  	destination=(Node)(nodeDictionary.get(
		      		new Integer(loadData.destNodeId)));
		}

	}
	
	public class PEntry implements XMLSerializable, TwoStageLoader
	{
		Sign tl;
		int pos;
		Node destination;
		boolean light;
		Sign tl_new;
		int pos_new;
		float value;
		TwoStageLoaderData loadData=new TwoStageLoaderData();
		String parentName="model.tlc";
		
		PEntry(Sign _tl, int _pos, Node _destination, boolean _light, Sign _tl_new, int _pos_new) {
			tl = _tl;
			pos = _pos;
			destination = _destination;
			light = _light;
			tl_new = _tl_new;
			pos_new = _pos_new;
			value=0;
		}
		
		PEntry ()
		{	// Empty constructor for loading
		}
		
		public void setValue(float v) {
			value = v;
		}
		
		public float getValue() {
			return value;
		}

		public boolean equals(Object other) {
			if(other != null && other instanceof PEntry) {
				PEntry pnew = (PEntry) other;
				if(!pnew.tl.equals(tl)) return false;
				if(pnew.pos!=pos) return false;
				if(!pnew.destination.equals(destination)) return false;
				if(pnew.light!=light) return false;
				if(!pnew.tl_new.equals(tl_new)) return false;
				if(pnew.pos_new!=pos_new) return false;
				return true;
			}
			return false;
		}

		public float sameSource(CountEntry other) {
			if(other.tl.equals(tl) && other.pos == pos && other.light==light && other.destination.equals(destination)) {
				return value;
			}
			else {
				return -1;
			}
		}
		
		// XMLSerializable implementation of PEntry
		
		public void load (XMLElement myElement,XMLLoader loader) throws XMLTreeException,IOException,XMLInvalidInputException
		{	pos=myElement.getAttribute("pos").getIntValue();
		   	loadData.oldTlId=myElement.getAttribute("tl-id").getIntValue();
			loadData.destNodeId=myElement.getAttribute("destination").getIntValue();
		   	light=myElement.getAttribute("light").getBoolValue();
			loadData.newTlId=myElement.getAttribute("newtl-id").getIntValue();
			pos_new=myElement.getAttribute("new-pos").getIntValue();
			value=myElement.getAttribute("value").getFloatValue(); 
		}

		public XMLElement saveSelf () throws XMLCannotSaveException
		{ 	XMLElement result=new XMLElement("pval");
			result.addAttribute(new XMLAttribute("tl-id",tl.getId()));
			result.addAttribute(new XMLAttribute("pos",pos));
			result.addAttribute(new	XMLAttribute("destination",destination.getId()));
			result.addAttribute(new XMLAttribute("light",light));
			result.addAttribute(new XMLAttribute("newtl-id",tl_new.getId()));
			result.addAttribute(new XMLAttribute("new-pos",pos_new));
			result.addAttribute(new XMLAttribute("value",value));
	  		return result;
		}
  
		public void saveChilds (XMLSaver saver) throws XMLTreeException,IOException,XMLCannotSaveException
		{ 	// A P-entry has no child objects
		}

		public String getXMLName ()
		{ 	return parentName+".pval";
		}
		
		public void setParentName (String parentName)
		{	this.parentName=parentName; 
		}
				
		// TwoStageLoader implementation of PEntry

		class TwoStageLoaderData 
		{  	int oldTlId,newTlId,destNodeId;
		}
		
		public void loadSecondStage (Dictionary dictionaries)
		{ 	Dictionary laneDictionary=(Dictionary)(dictionaries.get("lane")),
           		     nodeDictionary=(Dictionary)(dictionaries.get("node"));
		  	tl=((Drivelane)(laneDictionary.get(
		       		new Integer(loadData.oldTlId)))).getSign();
		  	tl_new=((Drivelane)(laneDictionary.get(
		        	new Integer(loadData.newTlId)))).getSign();
		  	destination=(Node)(nodeDictionary.get(
		        	new Integer(loadData.destNodeId)));
		}
		
	}	
	
	public class QEntry implements XMLSerializable, TwoStageLoader
	{
		Sign tl;
		int pos;
		Node destination;
		boolean light;
		float value;
		TwoStageLoaderData loadData=new TwoStageLoaderData();
		String parentName="model.tlc";
		
		QEntry(Sign _tl, int _pos, Node _destination, boolean _light) {
			tl = _tl;
			pos = _pos;
			destination = _destination;
			light = _light;
			value=0;
		}
		
		QEntry ()
		{ 	// Empty constructor for loading
		}
		
		public void setValue(float v) {
			value = v;
		}
		
		public float getValue() {
			return value;
		}

		public boolean equals(Object other) {
			if(other != null && other instanceof QEntry)
			{
				QEntry qnew = (QEntry) other;
				if(!qnew.tl.equals(tl)) return false;
				if(qnew.pos!=pos) return false;
				if(!qnew.destination.equals(destination)) return false;
				if(qnew.light!=light) return false;
				return true;
			}
			return false;
		}
		
		// XMLSerializable implementation of QEntry
		
		public void load (XMLElement myElement,XMLLoader loader) throws XMLTreeException,IOException,XMLInvalidInputException
		{	pos=myElement.getAttribute("pos").getIntValue();
		   	loadData.tlId=myElement.getAttribute("tl-id").getIntValue();
			loadData.destNodeId=myElement.getAttribute("destination").getIntValue();
		   	light=myElement.getAttribute("light").getBoolValue();
			value=myElement.getAttribute("value").getFloatValue(); 
		}
		

		public XMLElement saveSelf () throws XMLCannotSaveException
		{ 	XMLElement result=new XMLElement("qval");
			result.addAttribute(new XMLAttribute("tl-id",tl.getId()));
			result.addAttribute(new XMLAttribute("pos",pos));
			result.addAttribute(new	XMLAttribute("destination",destination.getId()));
			result.addAttribute(new XMLAttribute("light",light));
			result.addAttribute(new XMLAttribute("value",value));
	  		return result;
		}
  
		public void saveChilds (XMLSaver saver) throws XMLTreeException,IOException,XMLCannotSaveException
		{ 	// A Q-entry has no child objects
		}

		public String getXMLName ()
		{ 	return parentName+".qval";
		}
		
		public void setParentName (String parentName)
		{	this.parentName=parentName; 
		}
	
		// TwoStageLoader implementation of QEntry

		class TwoStageLoaderData 
		{ 	int tlId,destNodeId;
		}
		
		public void loadSecondStage (Dictionary dictionaries)
		{ 	Dictionary laneDictionary=(Dictionary)(dictionaries.get("lane")),
            		     nodeDictionary=(Dictionary)(dictionaries.get("node"));
		  	tl=((Drivelane)(laneDictionary.get(
		                 new Integer(loadData.tlId)))).getSign();
		  	destination=(Node)(nodeDictionary.get(
		                 new Integer(loadData.destNodeId)));
		}
		
	}		

	public class VEntry implements XMLSerializable, TwoStageLoader
	{
		Sign tl;
		int pos;
		Node destination;
		float value;
		TwoStageLoaderData loadData=new TwoStageLoaderData();
		String parentName="model.tlc";
		
		VEntry(Sign _tl, int _pos, Node _destination) {
			tl = _tl;
			pos = _pos;
			destination = _destination;
			value=0;
		}
		
		VEntry ()
		{ // Empty constructor for loading
		}
		
		public void setValue(float v) {
			value = v;
		}
		
		public float getValue() {
			return value;
		}

		public boolean equals(Object other) {			
			if(other != null && other instanceof VEntry) {
				VEntry qnew = (VEntry) other;
				if(qnew.tl.getId() != tl.getId()) return false;
				if(qnew.pos != pos) return false;
				if(qnew.destination.getId() != destination.getId()) return false;
				return true;
			}
			return false;
		}
		
		// XMLSerializable implementation of VEntry
		
		public void load (XMLElement myElement,XMLLoader loader) throws XMLTreeException,IOException,XMLInvalidInputException
		{	pos=myElement.getAttribute("pos").getIntValue();
		   	loadData.tlId=myElement.getAttribute("tl-id").getIntValue();
			loadData.destNodeId=myElement.getAttribute("destination").getIntValue();
			value=myElement.getAttribute("value").getFloatValue(); 
		}

		public XMLElement saveSelf () throws XMLCannotSaveException
		{ 	XMLElement result=new XMLElement("vval");
			result.addAttribute(new XMLAttribute("tl-id",tl.getId()));
			result.addAttribute(new XMLAttribute("pos",pos));
			result.addAttribute(new	XMLAttribute("destination",destination.getId()));
			result.addAttribute(new XMLAttribute("value",value));
	  		return result;
		}
  
		public void saveChilds (XMLSaver saver) throws XMLTreeException,IOException,XMLCannotSaveException
		{ 	// A V-entry has no child objects
		}

		public String getXMLName ()
		{ 	return parentName+".vval";
		}
		
		public void setParentName (String parentName)
		{	this.parentName=parentName; 
		}
		
		// TwoStageLoader implementation of VEntry

		class TwoStageLoaderData 
		{ 	int tlId,destNodeId;
		}
		
		public void loadSecondStage (Dictionary dictionaries)
		{ 	Dictionary laneDictionary=(Dictionary)(dictionaries.get("lane")),
        		     nodeDictionary=(Dictionary)(dictionaries.get("node"));
		 	tl=((Drivelane)(laneDictionary.get(
		                  new Integer(loadData.tlId)))).getSign();
		  	destination=(Node)(nodeDictionary.get(
		                  new Integer(loadData.tlId)));
		}
		
	}	
	
	protected class Target implements XMLSerializable , TwoStageLoader
	{
		Sign tl;
		int pos;
		TwoStageLoaderData loadData=new TwoStageLoaderData();
		String parentName="model.tlc";
		
		Target(Sign _tl, int _pos) {
			tl = _tl;
			pos = _pos;
		}
		
		Target ()
		{ // Empty constructor for loading
		}
		
		public Sign getTL() {
			return tl;
		}
		
		public int getP() {
			return pos;
		}

		public boolean equals(Object other) {
			if(other != null && other instanceof Target) {
				Target qnew = (Target) other;
				if(!qnew.tl.equals(tl)) return false;
				if(qnew.pos!=pos) return false;
				return true;
			}
			return false;
		}
		
		// XMLSerializable implementation of Target
		
		public void load (XMLElement myElement,XMLLoader loader) throws XMLTreeException,IOException,XMLInvalidInputException
		{	pos=myElement.getAttribute("pos").getIntValue();
		   	loadData.tlId=myElement.getAttribute("tl-id").getIntValue();
		}
		
		public XMLElement saveSelf () throws XMLCannotSaveException
		{ 	XMLElement result=new XMLElement("target");
			result.addAttribute(new XMLAttribute("tl-id",tl.getId()));
			result.addAttribute(new XMLAttribute("pos",pos));
	  		return result;
		}
  
		public void saveChilds (XMLSaver saver) throws XMLTreeException,IOException,XMLCannotSaveException
		{ 	// A Target has no child objects
		}

		public String getXMLName ()
		{ 	return parentName+".target";
		}
		
		public void setParentName (String parentName)
		{	this.parentName=parentName; 
		}
		
		// TwoStageLoader implementation of Target

		class TwoStageLoaderData 
		{ 	int tlId;
		}
		
		public void loadSecondStage (Dictionary dictionaries)
		{ 	Dictionary laneDictionary=(Dictionary)(dictionaries.get("lane"));
		  	tl=((Drivelane)(laneDictionary.get(
		                 new Integer(loadData.tlId)))).getSign();
		}
		
	}		

	public class PKtlEntry implements XMLSerializable, TwoStageLoader
	{
		Sign tl;
		int pos;
		Node destination;
		boolean light;
		Sign tl_new;
		int pos_new;
		float value;
		int Ktl;
		TwoStageLoaderData loadData=new TwoStageLoaderData();
		String parentName="model.tlc";
		
		PKtlEntry(Sign _tl, int _pos, Node _destination, boolean _light, Sign _tl_new, int _pos_new, int _Ktl) {
			tl = _tl;
			pos = _pos;
			destination = _destination;
			light = _light;
			tl_new = _tl_new;
			pos_new = _pos_new;
			value=0;
			Ktl = _Ktl;
		}
		
		PKtlEntry ()
		{	// Empty constructor for loading
		}
		
		public void setValue(float v) {
			value = v;
		}
		
		public float getValue() {
			return value;
		}

		public boolean equals(Object other) {
			if(other != null && other instanceof PKtlEntry) {
				PKtlEntry pnew = (PKtlEntry) other;
				if(!pnew.tl.equals(tl)) return false;
				if(pnew.pos!=pos) return false;
				if(!pnew.destination.equals(destination)) return false;
				if(pnew.light!=light) return false;
				if(!pnew.tl_new.equals(tl_new)) return false;
				if(pnew.pos_new!=pos_new) return false;
				if(pnew.Ktl!=Ktl) return false;
				return true;
			}
			return false;
		}

		public float sameSource(CountEntry other) {
			if(other.tl.equals(tl) && other.pos == pos && other.light==light && other.destination.equals(destination)) {
				return value;
			}
			else {
				return -1;
			}
		}
		
		// XMLSerializable implementation of PKtlEntry
		
		public void load (XMLElement myElement,XMLLoader loader) throws XMLTreeException,IOException,XMLInvalidInputException
		{	pos=myElement.getAttribute("pos").getIntValue();
		   	loadData.oldTlId=myElement.getAttribute("tl-id").getIntValue();
			loadData.destNodeId=myElement.getAttribute("destination").getIntValue();
		   	light=myElement.getAttribute("light").getBoolValue();
			loadData.newTlId=myElement.getAttribute("newtl-id").getIntValue();
			pos_new=myElement.getAttribute("new-pos").getIntValue();
			Ktl=myElement.getAttribute("ktl").getIntValue();
			value=myElement.getAttribute("value").getFloatValue(); 
		}

		public XMLElement saveSelf () throws XMLCannotSaveException
		{ 	XMLElement result=new XMLElement("pktlval");
			result.addAttribute(new XMLAttribute("tl-id",tl.getId()));
			result.addAttribute(new XMLAttribute("pos",pos));
			result.addAttribute(new	XMLAttribute("destination",destination.getId()));
			result.addAttribute(new XMLAttribute("light",light));
			result.addAttribute(new XMLAttribute("newtl-id",tl_new.getId()));
			result.addAttribute(new XMLAttribute("new-pos",pos_new));
			result.addAttribute(new XMLAttribute("value",value));
			result.addAttribute(new XMLAttribute("ktl",Ktl));
	  		return result;
		}
  
		public void saveChilds (XMLSaver saver) throws XMLTreeException,IOException,XMLCannotSaveException
		{ 	// A Pktl-entry has no child objects
		}

		public String getXMLName ()
		{ 	return parentName+".pktlval";
		}
		
		public void setParentName (String parentName)
		{	this.parentName=parentName; 
		}
			
		// TwoStageLoader implementation of PktlEntry

		class TwoStageLoaderData 
		{ 	int oldTlId,newTlId,destNodeId;
		}
		
		public void loadSecondStage (Dictionary dictionaries) throws XMLInvalidInputException,XMLTreeException
		{ 	Dictionary laneDictionary=(Dictionary)(dictionaries.get("lane")),
           		     nodeDictionary=(Dictionary)(dictionaries.get("node"));
		  	tl=((Drivelane)(laneDictionary.get(
		       		new Integer(loadData.oldTlId)))).getSign();
		  	tl_new=((Drivelane)(laneDictionary.get(
		        	new Integer(loadData.newTlId)))).getSign();
		  	destination=(Node)(nodeDictionary.get(
		        	new Integer(loadData.destNodeId)));
		}
		
	}	

	public void showSettings(Controller c)
	{
		String[] descs = {"Gamma (discount factor)", "Random decision chance"};
		float[] floats = {gamma, random_chance};
		TLCSettings settings = new TLCSettings(descs, null, floats);
				
		settings = doSettingsDialog(c, settings);
		gamma = settings.floats[0];
		random_chance = settings.floats[1];
	}
 	
	
	
	// XMLSerializable, SecondStageLoader and InstantiationAssistant implementation
	
	public void load (XMLElement myElement,XMLLoader loader) throws XMLTreeException,IOException,XMLInvalidInputException
	{	super.load(myElement,loader);
		q_table=(float[][][][])XMLArray.loadArray(this,loader);
		v_table=(float[][][])XMLArray.loadArray(this,loader);
		gamma=myElement.getAttribute("gamma").getFloatValue();
		random_chance=myElement.getAttribute("random-chance").getFloatValue();
		count=(Vector[][][])XMLArray.loadArray(this,loader,this);
		p_table=(Vector[][][])XMLArray.loadArray(this,loader,this);
		pKtl_table=(Vector[][][])XMLArray.loadArray(this,loader,this);
	}
		      
	public XMLElement saveSelf () throws XMLCannotSaveException
	{ 	XMLElement result=super.saveSelf();
		result.setName(shortXMLName);
		result.addAttribute(new XMLAttribute("random-chance",random_chance));
		result.addAttribute(new XMLAttribute("gamma",gamma));
	  	return result;
	}
  
	public void saveChilds (XMLSaver saver) throws XMLTreeException,IOException,XMLCannotSaveException
	{	super.saveChilds(saver);
		XMLArray.saveArray(q_table,this,saver,"q-table");
		XMLArray.saveArray(v_table,this,saver,"v-table");
		XMLArray.saveArray(count,this,saver,"count");
		XMLArray.saveArray(p_table,this,saver,"p-table");
		XMLArray.saveArray(pKtl_table,this,saver,"pKtl_table");
	}

	public String getXMLName ()
	{ 	return "model."+shortXMLName;
	}
		
	public void loadSecondStage (Dictionary dictionaries) throws XMLInvalidInputException,XMLTreeException
	{ 	for(int i=0;i<count.length;i++)
			for(int j=0;j<count[i].length;j++)
				for(int k=0;k<count[i][j].length;k++)
					XMLUtils.loadSecondStage(count[i][j][k].elements(),dictionaries);
		for(int i=0;i<p_table.length;i++)
			for(int j=0;j<p_table[i].length;j++)
				for(int k=0;k<p_table[i][j].length;k++)
					XMLUtils.loadSecondStage(p_table[i][j][k].elements(),dictionaries);
		for(int i=0;i<pKtl_table.length;i++)
			for(int j=0;j<pKtl_table[i].length;j++)
				for(int k=0;k<pKtl_table[i][j].length;k++)
					XMLUtils.loadSecondStage(pKtl_table[i][j][k].elements(),dictionaries);
		System.out.println("TC2 second stage load finished.");			
	}
	
	public boolean canCreateInstance (Class request)
	{ 	return CountEntry.class.equals(request) ||
	        	PEntry.class.equals(request) ||
			PKtlEntry.class.equals(request);
	}
	
	public Object createInstance (Class request) throws 
	      ClassNotFoundException,InstantiationException,IllegalAccessException
	{ 	if (CountEntry.class.equals(request))
		{  return new CountEntry();
		}
		else if ( PEntry.class.equals(request))
		{ return new PEntry();
		}
		else if ( PKtlEntry.class.equals(request))
		{ return new PKtlEntry();
		}
		else
		{ throw new ClassNotFoundException
		  ("TC2 IntstantiationAssistant cannot make instances of "+
		   request);
		}
	}	
	
}
