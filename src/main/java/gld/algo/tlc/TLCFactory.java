
/*-----------------------------------------------------------------------
 * Copyright (C) 2001 Green Light District Team, Utrecht University 
 *
 * This program (Green Light District) is free software.
 * You may redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by
 * the Free Software Foundation (version 2 or later).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * See the documentation of Green Light District for further information.
 *------------------------------------------------------------------------*/

package gld.algo.tlc;
/** This class can be used to create instances of Traffic Light Controllers
  * for a specific infrastructure.
 */

import gld.infra.InfraException;
import gld.infra.Infrastructure; 
import gld.utils.StringUtils;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Random;
 
public class TLCFactory
{
	protected Infrastructure infra;
	protected Random random;

	protected static final int			
		RANDOM=0,         		
 		LONGEST_QUEUE=1,  		
		MOST_CARS=2,	      			
		TC_1=3,      			
		TC_2=4,				
		TC_3=5,       			
		ACGJ_1=6,	      				
		ACGJ_2=7,	      				 
		ACGJ_3=8,	      				
		BEST_FIRST=9,					
		RELATIVE_LONGEST_QUEUE=10, 
		RLD=11,				
		TC_1OPT=12,							
		TC_2OPT=13,							
		LOCAL=14,							
		GENNEURAL=15,						
		ACGJ_3_FV=16,				
		RLD2=17,
		IATRACOS=18;
		
	protected static final String[] tlcDescs = {
		"Random",
		"Longest Queue",
		"Most Cars",
		"TC-1",
		"TC-2",
		"TC-3",
		"ACGJ-1",
		"ACGJ-2",
		"ACGJ-3",
		"Best First",
		"Relative Longest Queue",
		"Red Light District",
		"TC-1 Speed Optimized",
		"TC-2 Speed Optimized",
		"Local Hillclimbing",
		"GenNeural",
		"ACGJ-3 Stupidified",
		"Red Light District 2",
		"iAtracos"
	};
	
	protected static final String[] xmlNames = {
		RandomTLC.shortXMLName,
		LongestQueueTLC.shortXMLName,
		MostCarsTLC.shortXMLName,
		TC1TLC.shortXMLName,
		TC2TLC.shortXMLName,
		TC3TLC.shortXMLName,
		ACGJ1.shortXMLName,
		ACGJ2.shortXMLName,
		ACGJ3.shortXMLName,
		BestFirstTLC.shortXMLName,
		RelativeLongestQueueTLC.shortXMLName,
		RLDTLC.shortXMLName,
		TC1TLCOpt.shortXMLName,
		TC2TLCOpt.shortXMLName,
		LocalHillTLC.shortXMLName,
		GenNeuralTLC.shortXMLName,
		ACGJ3FixedValue.shortXMLName,
		RLD2TLC.shortXMLName,
		IATRACOSTLC.shortXMLName
	};
		
		
	protected static final String[] categoryDescs = {"Simple Maths", "Complex Maths", "Longest Q-variants", "Reinforcement Learning", "Genetic", "Neural Network"};
	protected static final int[][] categoryTLCs = {
		{RANDOM, MOST_CARS, RLD, RLD2, IATRACOS},
		{LOCAL, ACGJ_2},
		{LONGEST_QUEUE, RELATIVE_LONGEST_QUEUE, BEST_FIRST},
		{ TC_1, TC_2, TC_3, TC_1OPT, TC_2OPT },
		{ACGJ_1, ACGJ_3, ACGJ_3_FV},
		{GENNEURAL}
	};

	/** Makes a new TLCFactory for a specific infrastructure with a new
	  * random number generator.
	  * @param infra The infrastructure
	 */
  	public TLCFactory(Infrastructure infra)
	{ 	this.infra=infra;
		random=new Random();
	}
  
	/** Makes a new TLCFactory for a specific infrastructure 
	  * @param random The random number generator which some algorithms use
	  * @param infra The infrastructure
	 */
  	public TLCFactory(Infrastructure infra, Random random)
  	{ 	this.infra=infra;
		this.random=random;
  	}
  
	/** Looks up the id of a TLC algorithm by its description
	  * @param algoDesc The description of the algorithm
	  * @returns The id of the algorithm
	  * @throws NoSuchElementException If there is no algorithm with that
	  *        description.
	 */
	public static int getId (String algoDesc)
	{ 	return StringUtils.getIndexObject(tlcDescs,algoDesc);
	}
	
	/** Returns an array of TLC descriptions */
	public static String[] getTLCDescriptions() { return tlcDescs; }
  
  	/** Look up the description of a TLC algorithm by its id 
	  * @param algoId The id of the algorithm
	  * @returns The description
	  * @throws NoSuchElementException If there is no algorithm with the
	  *	    specified id.
	*/
  	public static String getDescription (int algoId)
  	{ 	return (String)(StringUtils.lookUpNumber(tlcDescs,algoId));
  	}
  
  	/** Returns an array containing the TLC category descriptions. */
  	public static String[] getCategoryDescs() 
	{ 	return categoryDescs; 
	}
	
  	/** Returns an array of TLC numbers for each TLC category. */
  	public static int[][] getCategoryTLCs() 
	{ 	return categoryTLCs; 
	}
	
  	/** Returns the total number of TLCs currently available. */
 	public static int getNumberOfTLCs() 
	{ 	return tlcDescs.length; 
	}

  	/** Gets the number of an algorithm from its XML tag name */
  	public int getNumberByXMLTagName(String tagName) 
  	{ 	return StringUtils.getIndexObject(xmlNames,tagName);
  	}
  
  	/** Returns an instance of a TLC by its description. */
  	public TLController genTLC (String tlcDesc) throws InfraException
	{	return getInstanceForLoad(getId(tlcDesc));
	}
	
	public TLController genTLC(int cat, int tlc) throws InfraException
	{
		return getInstanceForLoad(categoryTLCs[cat][tlc]);
	}
  
  	/** Gets a new instance of an algorithm by its number. This method
    	  * is meant to be used for loading.
   	*/
	public TLController getInstanceForLoad (int algoId) throws InfraException
	{
		switch (algoId)
	    	{ 	case RANDOM : return (random != null ? new RandomTLC(infra, random): new RandomTLC(infra));
			case LONGEST_QUEUE : return new LongestQueueTLC(infra);
			case TC_1 : return new TC1TLC(infra);
			case TC_2 : return new TC2TLC(infra);
			case TC_3 : return new TC3TLC(infra);
			case ACGJ_1 : return new ACGJ1(infra);		
			case ACGJ_2 : return new ACGJ2(infra);
			case ACGJ_3 : return new ACGJ3(infra);
			case BEST_FIRST : return new BestFirstTLC(infra);
			case RELATIVE_LONGEST_QUEUE : return new RelativeLongestQueueTLC(infra);
			case RLD : return new RLDTLC(infra);
			case MOST_CARS : return new MostCarsTLC(infra);
			case LOCAL : return new LocalHillTLC(infra);
			case GENNEURAL : return new GenNeuralTLC(infra);
			case TC_1OPT : return new TC1TLCOpt(infra);
			case TC_2OPT : return new TC2TLCOpt(infra);
			case ACGJ_3_FV : return new ACGJ3FixedValue(infra);
			case RLD2: return new RLD2TLC(infra);
			case IATRACOS: return new IATRACOSTLC(infra);
		}
	   	throw new InfraException
    			("The TLCFactory can't make TLC's of type "+algoId);
	}
  
}  
