/*
 * Created on Sep 5, 2004
 * Universidad Tecnológica Nacional - Facultad Regional Buenos Aires
 * Proyecto Final 2004
 * I-ATraCoS - Grupo :
 * - Brey, Gustavo
 * - Escobar, Gaston
 * - Espinosa, Marisa
 * - Pastorino, Marcelo
 * All right reserved
 * Class Description
 * 
 */
package gld.algo.tlc.iatracos.listener;

import java.util.ResourceBundle;

/**
* CVS Log
* $Id: SignConfigListenerFactory.java,v 1.3 2004/10/25 14:31:39 iatracos Exp $
* @author $Author: iatracos $
* @version $Revision: 1.3 $
**/
public class SignConfigListenerFactory {
	private static String classKey = "gld.algo.tlc.iatracos.listener.factory";

	private SignConfigListenerFactory() {
	}

	public static BaseSignConfigListener getSignConfigListener(ResourceBundle rb) {
		BaseSignConfigListener signConfigListener = null;
		try {
			signConfigListener =
				(BaseSignConfigListener) Class
					.forName(rb.getString(classKey))
					.newInstance();
			signConfigListener.setResourceBundle(rb);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return signConfigListener;
	}
}
